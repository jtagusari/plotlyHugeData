#' Wrapper for applying down-sampling
#'
#' @description
#' This function is an easy wrapper that employs
#' an R6 class \code{dash_downsampler} to obtain samples
#' from data using a specified aggregation method and
#' update the figures interactively.
#' The aggregation is determined according to the x-range of the figure
#' that user can define manually in the dash app.
#' @param obj Numeric vector, data.frame, plotly-class structure or
#' an instance of the R6 \code{Dash} class.
#' If a numeric vector is given, a specific down-sampling method will be
#' employed and a down-sampled vector will be returned.
#' If a data.frame is given, a specific down-sampling method will be employed
#' using \code{x} and \code{y} columns and a down-sampled data.frame
#' will be returned.
#' If a plotly or dash is given, dash application to which
#' a specific aggregator is applied will be returned and
#' down-sampling will be automatically applied according to the user's
#' operation of the dash app.
#' @param n_out Integer, optional.
#' Number of samples get by the down-sampling. By default, 1000.
#' @param aggregator R6 class for the aggregation, optional.
#' Select one out of
#' \code{LTTB_aggregator}, \code{min_max_ovlp_aggregator},
#' \code{min_max_aggregator}, \code{eLTTB_aggregator},
#' \code{nth_pnt_aggregator}, \code{custom_stat_aggregator},
#' \code{mean_aggregator}, \code{median_aggregator},
#' \code{min_aggregator}, \code{max_aggregator},
#' or \code{custom_func_aggregator}.
#' By default \code{eLTTB_aggregator}.
#' @param downsampler_options Named list, optional.
#' Arguments passed to \code{dash_downsampler$new} or the constructor of
#' the specific aggregator.
#' To set \code{aggregator} and \code{n_shown_samples},
#' use \code{aggregator} and \code{n_out} arguments.
#' @param dash_runserver_options Named list, optional.
#' Arguments passed to \code{run_server} method of the Dash app.
#' @param traceupdater_options Named list, optional.
#' Arguments passed to \code{trace_updater} function
#' other than \code{id} and \code{gdID}.
#' @param run_dash Boolean, optional.
#' When the \code{obj} is a plotly structure, this indicates
#' whether a generated dash app will be run immediately.
#' @param graph_id Character.
#' When the \code{obj} is an instance of Dash, this indicates
#' the id of the graph to which the down-sampling is applied.
#' @param ... Not used.
#' @export
#' @examples
#' data(noise_fluct)
#'
#' y_agg <- apply_downsampler(noise_fluct$level)
#'
#' d_agg <- noise_fluct %>%
#'   dplyr::select(x = sec, y = level) %>%
#'   apply_downsampler()
#'
#'\dontrun{
#' plotly::plot_ly(noise_fluct) %>%
#'   plotly::add_trace(x = ~sec, y = ~level, type = "scatter", mode = "lines") %>%
#'   apply_downsampler()
#'
#' app <- dash::dash_app()
#' app %>%
#'   dash::set_layout(
#'     dccGraph(
#'       id = "level-graph",
#'       plotly::plot_ly(noise_fluct) %>%
#'         plotly::add_trace(
#'           x = ~sec, y = ~level, type = "scatter", mode = "lines"
#'         )
#'     )
#'   ) %>%
#'   apply_downsampler(graph_id = "level-graph")
#'}
apply_downsampler <- function(obj, ...) {
  UseMethod("apply_downsampler", obj)
}


#' @rdname apply_downsampler
#' @export
apply_downsampler.numeric <- function(
    obj,
    n_out = 1000L,
    aggregator = eLTTB_aggregator,
    downsampler_options = list(),
    ...
) {
  agg <- aggregator$new()
  y_agg <- agg$aggregate(
    seq_along(obj), obj, n_out
  )

  return(y_agg$y)
}

#' @rdname apply_downsampler
#' @export
apply_downsampler.data.frame <- function(
  obj,
  n_out = 1000L,
  aggregator = eLTTB_aggregator,
  downsampler_options = list(),
  ...
  ) {
  agg <- aggregator$new()
  d_agg <- agg$aggregate(
    obj$x, obj$y, n_out
    ) %>%
    tibble::as_tibble()
  return(d_agg)
}

#' @rdname apply_downsampler
#' @export
apply_downsampler.plotly <- function(
  obj,
  n_out = 1000L,
  aggregator = eLTTB_aggregator,
  run_dash = TRUE,
  downsampler_options = list(),
  dash_runserver_options = list(use_viewer = TRUE),
  traceupdater_options = list(sequentialUpdate = FALSE),
  ...
  ) {

  assertthat::assert_that(
    inherits(obj, "plotly"),
    msg = "input p must be plotly class"
  )

  downsampler_options[["n_out"]] <- n_out
  downsampler_options[["aggregator"]] <- aggregator$new()

  fd <- do.call(
    dash_downsampler$new,
    c(list(figure = obj), downsampler_options)
  )

  if (run_dash) {
    do.call(
      fd$show_dash,
      c(
        list(dash_runserver_options),
        list(traceupdater_options)
      )
    )
  }

  invisible(fd)
}


#' @rdname apply_downsampler
#' @export
apply_downsampler.Dash <- function(
  obj,
  n_out = 1000L,
  aggregator = eLTTB_aggregator,
  graph_id,
  downsampler_options = list(),
  traceupdater_options = list(sequentialUpdate = FALSE),
  ...
  ) {

  # check data type
  assertthat::assert_that(inherits(obj, "Dash"))

  # get the app layout
  layout <- obj$layout_get(FALSE)

  # get the figure to which the downsampling is applied
  if (is.null(layout$props$children)) {
    dash_components <- list(layout)
  } else {
    dash_components <- layout$props$children
  }

  ids <- purrr::map_chr(dash_components, ~.x$props$id)

  # check that the graph_id exists and the type of the component is Graph
  assertthat::assert_that(
    graph_id %in% ids,
    msg = "The given graph_id does not exist in the app."
  )
  assertthat::assert_that(
    dash_components[[which(ids == graph_id)]]$type == "Graph",
    msg = "The type of the graph_id component is NOT 'Graph'."
  )

  downsampler_options[["n_out"]] <- n_out
  downsampler_options[["aggregator"]] <- aggregator$new()

  # create dash_downsampler instance and
  fd <- do.call(
    dash_downsampler$new,
    c(
      list(figure = dash_components[[which(ids == graph_id)]]$props$figure),
      downsampler_options
    )
  )

  # substitute the dash components layout with the dash_downsampler figure
  dash_components[[which(ids == graph_id)]]$props$figure <- fd$figure

  # set layout
  fd$app <- do.call(
    dash::set_layout,
    c(app = list(dash::dash_app()), dash_components)
  )

  # register trace updater
  fd$register_traceupdater(graph_id)

  return(fd)
}
