#' Time-series fluctuations in sound level
#'
#' @name noise_fluct
#' @docType data
#' @author Junta Tagusari \email{j.tagusari@@eng.hokudai.ac.jp}
#' @keywords time-series sound noise traffic
#' @description
#' Results of the measurement of the sound level,
#' where peaks due to road traffic are observed.
NULL
