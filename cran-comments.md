## Test environments
─ local x86_64-w64-mingw32 (64-bit), R 4.2.0
─ local x86_64-w64-mingw32 (64-bit), R 4.2.1
- gitlab x86_64-pc-linux-gnu (64-bit), R 4.2.1

## R CMD check results
There were no ERRORs, WARNINGs, or NOTEs.

## Downstream dependencies
There are no downstream dependencies.
