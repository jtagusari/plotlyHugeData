
# plotlyHugeData

The goal of plotlyHugeData is to efficiently plot the data of which
sample size is very large. Using this package, small number of samples
are obtained from huge-sized data automatically. Moreover, it can
interactively change the samples according to the plot range the user
defined.

For instance, assume that there is a data of which sample size is 1e8.

Without this package, to plot the overall data takes much time. It is
difficult to study the macro and micro structure of the data because of
the large sample size and the resolution of the chart. To divide the
data into intervals and calculate statistical values such as mean may be
a good approach for understanding the macro trend of the data; however,
the micro structure will be lost and it is necessary to extract a part
of the data and plot it.

Using this package, the automatically obtained samples are plotted
quickly, which help understand the macro structure of the data.
Moreover, zooming up the data provides new samples and illustrates the
micro structure of the data. Both the macro and micro structures of the
data can be easily and accurately understood by this package.

## Installation

You can install plotlyHugeData from CRAN like so: (As of 2022/7/19, this
package is not registered in CRAN)

``` r
# install.packages("plotlyHugeData")
```

Or you can install the developing version of the package from gitlab
like so:

``` r
install.packages("remotes")
remotes::install_gitlab("jtagusari/plotlyHugeData")
```

## NOTE

This package is largely dependent on `dash` package; however, it was
removed from the CRAN and cannot install for R (\>= 4.2.1). Therefore,
this package works only in R version 4.2.0 (or perhaps 4.x.x) at this
moment!

## Example

This is a basic example.

A dash app will be running in the R-studio viewer. The original data has
1e6 samples while only 1e3 samples are shown to reduce the processing
time. Try zooming up the plot and confirm that more samples are shown
according to the zoom.

Note that how much the samples are down-sampled are shown in the
legends. For example, the legend name of `[R] trace 1 ~1.0K` means that
the name of the displayed series is “trace 1” and that one sample is
generated for every 1.0K (= 1000) samples using a particular
down-sampling algorithm.

``` r
library(tidyverse)
library(plotlyHugeData)

d <- tibble::tibble(
  x = seq(0, 1e6),
  t = nanotime::nanotime(Sys.time()) + seq(0, 1e6) * 7e4,
  tp = Sys.time() + seq(0, 1e6) * 7,
  tch = format(t, "%Y-%m-%d %H:%M:%E9S"),
  y = (3 + sin(x / 200) + runif(1e6 + 1) / 10) * x / 1000
)

plotly::plot_ly() %>%
  add_trace(x = d$x, y = d$y, type = "scatter", mode = "lines") %>%
  apply_downsampler()
```

Here is another example, using dash explicitly and datetime axis.

``` r
app <- dash::dash_app() %>%
  dash::set_layout(
    dash::dccGraph(
      id = "downsample-figure",
      figure = plotly::plot_ly(
        data = d, x = ~t, y = ~y, type = "scatter", mode = "lines"
      )
    ),
    dash::dccGraph(
      id = "other-figure",
      figure = plotly::plot_ly(
        data = d[1:1e2,], x = ~x, y = ~y - 10,
        type = "scatter", mode = "lines"
      )
    )
  ) %>%
  apply_downsampler(graph_id = "downsample-figure")

app$run_server(host = "127.0.0.1", port = "8050", use_viewer = TRUE)
```

## LICENSE

See `LICENSE` and `LICENSE.note`. This package includes other open
source software components.
